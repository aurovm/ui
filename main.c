#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <ffi.h>

struct Opt { size_t Size; };

void (*uiQuit)();

int onClosing(void *w, void *data) {
    //uiQuit();
    printf("OnClosing\n");
    return 1;
}

int onShouldQuit(void *data) {
    printf("onShouldQuit\n");
    //uiWindow *mainwin = uiWindow(data);
    //uiControlDestroy(uiControl(mainwin));
    return 1;
}

void callback(ffi_cif *cif, void *ret, void* args[], void *ctx) {
    printf("Closing Callback. Ctx: %d\n", *(int*)ctx);
    uiQuit();
    *(int*)ret = 1;
}

int main(int argc, char **argv) {
    void *handle;

    void *(*uiNewWindow)(char*, int, int, int);
    void *(*uiInit)(struct Opt*);
    void (*uiMain)();
    void (*uiControlShow)(void *);

    void (*uiOnShouldQuit)(void *f, void *data);
    void (*uiWindowOnClosing)(void *w, void *f, void *data);

    char *error;

    handle = dlopen ("/usr/lib/libui.so", RTLD_LAZY);

    printf("int: %d, long: %d, long long: %d, void*: %d\n", sizeof(int), sizeof(long), sizeof(long long), sizeof(void*));

    uiNewWindow = dlsym(handle, "uiNewWindow");
    uiInit = dlsym(handle, "uiInit");
    uiMain = dlsym(handle, "uiMain");
    uiQuit = dlsym(handle, "uiQuit");
    uiControlShow = dlsym(handle, "uiControlShow");
    uiOnShouldQuit = dlsym(handle, "uiOnShouldQuit");
    uiWindowOnClosing = dlsym(handle, "uiWindowOnClosing");

    struct Opt opt = { sizeof (struct Opt) };
    char *err = uiInit(&opt);

    void *win = uiNewWindow(0, 640, 380, 0);


    //=== Closing callback start ===//

        ffi_cif cif;
        ffi_type *args[2];
        void *values[1];
        char *s;
        ffi_arg rc;


        ffi_closure *closure;

        void *bound;

        /* Allocate closure and bound */
        closure = ffi_closure_alloc(sizeof(ffi_closure), &bound);

        if (!closure) {
            printf("Something went wrong allocating the closure");
            exit(1);
        }

        /* Initialize the argument info vectors */
        args[0] = &ffi_type_pointer;
        args[1] = &ffi_type_pointer;

        /* Initialize the cif */
        if (ffi_prep_cif(&cif, FFI_DEFAULT_ABI, 1, &ffi_type_sint32, args) != FFI_OK) {
            printf("Something went wrong with preparing the statement");
            exit(1);
        }

        int ctx = 42;

        /* Initialize the closure, setting stream to stdout */
        if (ffi_prep_closure_loc(closure, &cif, (void*) callback, (void*) &ctx, bound) != FFI_OK) {
            printf("Something went wrong initializing the closure");
            exit(1);
        }

        uiWindowOnClosing(win, bound, NULL);

    //=== Closing callback end ===//

    //uiWindowOnClosing(win, onClosing, NULL);
    //uiOnShouldQuit(onShouldQuit, NULL);

    uiControlShow(win);
    uiQuit();
    //uiMain();

    dlclose(handle);
    return 0;
}