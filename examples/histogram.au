
module libui = import libui;
module float_arr_mod = (import auro.array)({ `0` = float; });

type float_arr = float_arr_mod.``;
float_arr new_float_arr (float, int) = float_arr_mod.`new`;
int float_arr_len (float_arr) = float_arr_mod.len;
float float_arr_get (float_arr, int) = float_arr_mod.get;
void float_arr_set (float_arr, int, float) = float_arr_mod.set;

module should_quit_mod = (import libui.should_quit)({ `0` = should_quit_fn; });
module window_closing_mod = (import libui.window_closing)({ `0` = window_closing_fn; });

import libui.Window;
import libui.Control;

type ShouldQuit = libui.ShouldQuit;
type WindowClosing = libui.WindowClosing;
type ButtonClicked = libui.ButtonClicked;

void init () = libui.init;
void ui_main () = libui.main;
void quit () = libui.quit;

void on_should_quit (ShouldQuit, any data) = libui.on_should_quit;
void window_on_closing (Window, WindowClosing, any data) = libui.window_on_closing;

ShouldQuit should_quit () = should_quit_mod.``;
int should_quit_fn (any data) {
    println("Should Quit");
    return 1;
}

WindowClosing window_closing () = window_closing_mod.``;
int window_closing_fn(Window win, any data) {
    println("Window Closing");
    quit();
    return 1;
}

import libui.Label;
import libui.Spinbox;
import libui.Box;
Box newVerticalBox () = libui.newVerticalBox;
Box newHorizontalBox () = libui.newHorizontalBox;

import libui.Area;
import libui.AreaDrawParams;

type AreaMouseEvent = libui.AreaMouseEvent;
type AreaKeyEvent = libui.AreaKeyEvent;

type AreaDrawHandler = libui.AreaDrawHandler;
type AreaMouseEventHandler = libui.AreaMouseEventHandler;
type AreaMouseCrossedHandler = libui.AreaMouseCrossedHandler;
type AreaDragBrokenHandler = libui.AreaDragBrokenHandler;
type AreaKeyEventHandler = libui.AreaKeyEventHandler;

import libui.AreaHandler;
import libui.DrawContext;
import libui.DrawBrush;
import libui.DrawPath;
import libui.DrawStrokeParams;

void fill (DrawContext c, DrawPath path, DrawBrush b) = libui.DrawFill;
void stroke (DrawContext c, DrawPath path, DrawBrush b, DrawStrokeParams p) = libui.DrawStroke;

DrawPath graph_path (Info info) {
    DrawPath path = new DrawPath();

    float x; float y;
    path.newFigure(20.0, info.height + 20.0);

    int i = 0;
    while (i < float_arr_len(info.points)) {
        x, y = info.location(i);
        path.lineTo(x, y);
        i = i+1;
    }
    path.lineTo(info.width + 20.0, info.height + 20.0);

    path.end();
    return path;
}

void draw_fn (AreaHandler this, Area area, AreaDrawParams drawParams) {
    Info info = this.getData() as Info;

    DrawPath path;
    DrawBrush brush = new DrawBrush();
    DrawStrokeParams sp = new DrawStrokeParams();

    float one = itof(1);
    float zero = itof(0);

    sp.Cap = 0;
    sp.Join = 0;
    sp.Thickness = 1.0;
    sp.MiterLimit = 2.0;

    brush.setSolid();
    brush.setA(one);

    // fill the area with white

    brush.setR(one);
    brush.setG(one);
    brush.setB(one);

    path = new DrawPath();
    path.addRectangle(
        itof(0),
        itof(0),
        drawParams.getAreaWidth(),
        drawParams.getAreaHeight()
    );
    path.end();

    fill(drawParams.getContext(), path, brush);
    path.free();

    // figure out dimensions
    float margin = itof(20);
    float width = drawParams.getAreaWidth() - (margin * 2.0);
    float height = drawParams.getAreaHeight() - (margin * 2.0);
    info.width = width;
    info.height = height;

    // draw the axes
    // black color
    brush.setR(zero);
    brush.setG(zero);
    brush.setB(zero);
    path = new DrawPath();
    path.newFigure(margin, margin);
    path.lineTo(margin, margin + height);
    path.lineTo(margin + width, margin + height);
    path.end();

    stroke(drawParams.getContext(), path, brush, sp);
    path.free();

    // Draw the graph
    path = graph_path(info);
    stroke(drawParams.getContext(), path, brush, sp);
    path.free();
}

module draw_handler_mod = (import libui.area_draw_handler)({ `0` = draw_fn; });
AreaDrawHandler drawHandler () = draw_handler_mod.``;

void mouse_event_fn (AreaHandler this, Area area, AreaMouseEvent mouseEvent) {}
module mouse_event_handler_mod = (import libui.area_mouse_event_handler)({ `0` = mouse_event_fn; });
AreaMouseEventHandler mouse_event_handler () = mouse_event_handler_mod.``;

void mouse_crossed_fn (AreaHandler this, Area area, int left) {}
module mouse_crossed_handler_mod = (import libui.area_mouse_crossed_handler)({ `0` = mouse_crossed_fn; });
AreaMouseCrossedHandler mouse_crossed_handler () = mouse_crossed_handler_mod.``;

void drag_broken_fn (AreaHandler this, Area area) {}
module drag_broken_handler_mod = (import libui.area_drag_broken_handler)({ `0` = drag_broken_fn; });
AreaDragBrokenHandler drag_broken_handler () = drag_broken_handler_mod.``;

int key_event_fn (AreaHandler this, Area area, AreaKeyEvent keyEvent) { return 0; }
module key_event_handler_mod = (import libui.area_key_event_handler)({ `0` = key_event_fn; });
AreaKeyEventHandler key_event_handler () = key_event_handler_mod.``;

module spinbox_changed_mod = (import libui.spinbox_changed)({ `0` = spinbox_changed_fn; });
type SpinboxChanged = libui.SpinboxChanged;
SpinboxChanged spinbox_changed () = spinbox_changed_mod.``;

record Info {
    float width;
    float height;
    float_arr points;

    Area? area;
}

record SpinboxInfo {
    Info info;
    int index;
}

void spinbox_changed_fn (Spinbox this, any data) {
    SpinboxInfo sinfo = data as SpinboxInfo;
    Info info = sinfo.info;
    //println("Changed i=" + itos(sinfo.index) + ", value=" + itos(this.value()));

    float_arr_set(info.points, sinfo.index, itof(this.value()) / 100.0);

    Area a = info.area as Area;
    a.queueRedrawAll();
}

type Info {
    void fill (Info info) {
        float_arr_set(info.points, 0, 0.4);
        float_arr_set(info.points, 1, 0.5);
        float_arr_set(info.points, 2, 0.1);
        float_arr_set(info.points, 3, 0.3);
        float_arr_set(info.points, 4, 0.8);
    }

    float, float location (Info this, int i) {
        /*float n = itof(float_arr_len(this.points));
        float x = float_arr_get(this.points, i);
        return 20.0 + ((itof(i)/n) * this.width), 20.0 + (1.0 - x * this.height);*/
        float n = itof(float_arr_len(this.points) + 1);
        float x = itof(i + 1) * this.width / n + 20.0;
        float v = float_arr_get(this.points, i);
        float y = (1.0 - v) * this.height + 20.0;

        return x, y;
    }

    int count (Info this) {
        return float_arr_len(this.points);
    }
}


void main () {
    init();
    Window win = new Window("Hello", 640, 480, false);

    window_on_closing(win, window_closing(), false as any);
    on_should_quit(should_quit(), false as any);

    Info info = new Info(itof(0), itof(0), new_float_arr(0.0, 5), null Area);

    info.fill();

    AreaHandler ah = new AreaHandler(info as any);
    ah.setDraw(drawHandler());

    ah.setMouseEvent(mouse_event_handler());
    ah.setMouseCrossed(mouse_crossed_handler());
    ah.setDragBroken(drag_broken_handler());
    ah.setKeyEvent(key_event_handler());

    Box hbox = newHorizontalBox();
    hbox.setPadded(true);
    win.setChild(hbox.toControl());

    Box vbox = newVerticalBox();
    vbox.setPadded(true);
    hbox.append(vbox.toControl(), false);

    int i = 0;
    while (i < info.count()) {
        SpinboxInfo sinfo = new SpinboxInfo(info, i);
        Spinbox sb = new Spinbox(0, 100);
        sb.setValue(ftoi(float_arr_get(info.points, i) * 100.0));
        sb.onChanged(spinbox_changed(), sinfo as any);
        vbox.append(sb.toControl(), false);
        i = i+1;
    }

    Area a = new Area(ah);
    hbox.append(a.toControl(), true);

    info.area = a as Area?;


    Control _c = win.toControl();
    _c.show();

    ui_main();
}
