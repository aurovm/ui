
if [ ! -d ffi-parser ]; then
    git submodule init --recursive
    git submodule update --recursive
fi

mkdir -p dist


if [ "$1" == "build" -o "$1" == "b" ]; then

    # Until it's implemented natively
    auro aulang src/ffic.au dist/auro.ffi.c &&

    python ffi-parser/main.py ui.h dist/libui.clib libui &&
    auro aulang src/callback_types.au dist/libui.callback_types &&

    auro aulang src/callbacks/should_quit.au dist/libui.should_quit &&
    auro aulang src/callbacks/window_closing.au dist/libui.window_closing &&
    auro aulang src/callbacks/button_clicked.au dist/libui.button_clicked &&
    auro aulang src/callbacks/spinbox_changed.au dist/libui.spinbox_changed &&
    auro aulang src/callbacks/area_draw_handler.au dist/libui.area_draw_handler &&
    auro aulang src/callbacks/area_mouse_event_handler.au dist/libui.area_mouse_event_handler &&
    auro aulang src/callbacks/area_mouse_crossed_handler.au dist/libui.area_mouse_crossed_handler &&
    auro aulang src/callbacks/area_drag_broken_handler.au dist/libui.area_drag_broken_handler &&
    auro aulang src/callbacks/area_key_event_handler.au dist/libui.area_key_event_handler &&

    auro aulang -L dist src/libui.au dist/libui

    exit
fi

if [ "$1" == "install" -o "$1" == "i" ]; then
    cd dist
    for a in $(ls); do
        auro --install $a
    done
    exit
fi

auro aulang -L dist src/libui.au dist/libui &&

auro aulang -L dist examples/histogram.au out &&
auro --dir dist out
